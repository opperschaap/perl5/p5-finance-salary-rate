use strict;
use warnings;
use Test::More 0.96;

use_ok("Finance::Salary::Rate");

my %args = (
    monthly_income       => 1750,
    vacation_perc        => 8,
    tax_perc             => 30,
    healthcare_perc      => 5.7,
    pension_perc         => 2.7,
    declarable_days_perc => 60,
    working_days         => 230,
    expenses             => 2000,
);

my $rate = Finance::Salary::Rate->new(%args);

isa_ok($rate, 'Finance::Salary::Rate');

is($rate->monthly_income, 1750,      "Montly income is alright");
is($rate->income,         1750 * 12 * 1.08, ".. and yearly is ok too");


is($rate->gross_income, $rate->income * 1.30, "Gross income");
is($rate->gross_income, $rate->get_tax_fee + $rate->income, "Gross income is income + taxes");

my $healthcare = $rate->get_healthcare_fee;
is(
    $healthcare,
    $rate->gross_income * 0.057,
    ".. plus healthcare"
);

my $pension = $rate->get_pension_fee;
is(
    $pension,
    $rate->gross_income * 0.027,
    ".. plus pension"
);

is($rate->get_social_fees, $pension + $healthcare,
    ".. adds up to social fees");
is(
    $rate->required_income,
    $rate->gross_income + $rate->expenses + $rate->get_social_fees,
    ".. and requires income"
);


is($rate->workable_hours, 230 * 8 * .6, "Workable hours is correct");

my $hr = $rate->hourly_rate;
is(
    $hr,
    $rate->required_income / $rate->workable_hours,
    "Pay up: $hr"
);

{
    my $rate = Finance::Salary::Rate->new(%args);

    my $new_rate = $rate->hourly_rate;
    is(
        $new_rate,
        $hr,
        "Pay up: $hr"
    );

    is($rate->declarable, .6, "Declarable is 60%");


}

done_testing;
