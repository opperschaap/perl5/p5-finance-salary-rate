use utf8;
package Finance::Salary::Rate;
our $VERSION = '0.002';
use Moo;
use Types::Standard qw(Num);
use namespace::autoclean;

# ABSTRACT: Calculate hourly rates for small businesses

has monthly_income => (
    is       => 'ro',
    isa      => Num,
    required => 1,
);

has vacation_perc => (
    is        => 'ro',
    isa       => Num,
    default   => 0,
);

has tax_perc => (
    is        => 'ro',
    isa       => Num,
    default   => 0,
);

has healthcare_perc => (
    is        => 'ro',
    isa       => Num,
    default   => 0,
);

has declarable_days_perc => (
    is        => 'ro',
    isa       => Num,
    default   => 60,
);

has rate => (
    is        => 'ro',
    isa       => Num,
    predicate => 'has_rate',
);

has working_days => (
    is        => 'ro',
    isa       => Num,
    default   => 230,
);

has expenses => (
    is        => 'ro',
    isa       => Num,
    default   => 0,
);

has pension_perc => (
    is        => 'ro',
    isa       => Num,
    default   => 0,
);

sub gross_income {
    my $self = shift;
    return $self->income + $self->get_tax_fee;
}

sub _get_perc {
    my ($self, $perc) = @_;
    return $perc / 100;
}

sub get_healthcare_fee {
    my $self = shift;
    return $self->gross_income * $self->healthcare;
}

sub get_pension_fee {
    my $self = shift;
    return $self->gross_income * $self->pension;
}

sub get_tax_fee {
    my $self = shift;
    return $self->income * $self->taxes;
}

sub workable_hours {
    my $self = shift;

    return $self->required_income / $self->rate if $self->has_rate;
    return $self->working_days * 8 * $self->declarable;
}

sub get_social_fees {
    my $self = shift;

    return $self->get_healthcare_fee + $self->get_pension_fee;
}

sub required_income {
    my $self = shift;
    return $self->gross_income + $self->get_social_fees + $self->expenses;
}

sub hourly_rate {
    my $self = shift;

    return $self->required_income / $self->workable_hours;
}

sub declarable {
    my $self = shift;
    if ($self->has_rate) {
        my $hrs = $self->workable_hours;
        my $days = $hrs / 8;
        return $days / $self->working_days;
    }
    return $self->_get_perc($self->declarable_days_perc);
}

sub vacation_pay {
    my $self = shift;
    return $self->_get_perc($self->vacation_perc)
}

sub taxes {
    my $self = shift;
    return $self->_get_perc($self->tax_perc)
}

sub healthcare {
    my $self = shift;
    return $self->_get_perc($self->healthcare_perc)
}

sub pension {
    my $self = shift;
    return $self->_get_perc($self->pension_perc)
}

sub income {
    my $self = shift;
    my $income = $self->monthly_income * 12;
    return $income * ( 1 + $self->vacation_pay );
}

1;

__END__

=head1 DESCRIPTION

A calculator to calculate hourly rates for small businesses and the likes.
Based on the Dutch
L<Ondernemersplein|https://ondernemersplein.kvk.nl/voorbeeld-uurtarief-berekenen/>
method.

=head1 SYNOPSIS

    my $rate = Finance::Salary::Rate->new(
        monthly_income       => 1750,
        vacation_perc        => 8,
        tax_perc             => 30,
        healthcare_perc      => 5.7,
        declarable_days_perc => 60,
        days                 => 230,
        expenses             => 2000,
    );

    print "My hourly rate is " . $rate->hourly_rate;

=head1 ATTRIBUTES

=head2 income

The monthly income you want to receive. Required.

=head2 vacation_perc

The percentage of what you want to pay yourself for vacation money. Optional.

=head2 tax_perc

The percentage of taxes you need to set aside for the government. Optional.

=head2 healthcare_perc

The percentage of income you need to set aside for health care insureance.
Optional.

=head2 healthcare_perc

The percentage of income you need to set aside for health care insureance.
Optional.

=head2 declarable_days_perc

The percentage of declarable days per week. Optional and defaults to 60%.

=head2 working_days

The total amount of working days in a year. Optional and defaults to 230.

=head2 expenses

Estimated expenses per year. Optional.

=head1 METHODS

=head2 monthly_income

Returns the montly income

=head2 yearly_income

Returns the yearly income

=head2 weekly_income

Returns the weekly income

=head2
